from django.db import models
from django.utils import timezone

# Create your models here.
class Album(models.Model):
    albumid = models.AutoField(primary_key=True)
    #albumimage = models.ImageField(upload_to='album/')
    albumimage = models.CharField(max_length=250,blank=True,null=True)
    description = models.TextField(blank=True, null=True)
    createdate = models.DateTimeField(auto_now_add=True, blank=True, null=True)
