from django.contrib import admin
from .models import Album

# Register your models here.
class AlbumAdmin(admin.ModelAdmin):
    list_display = ('albumid', 'albumimage', 'description', 'createdate')

# Register your models here.
admin.site.register(Album, AlbumAdmin)